<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Model\Admin\SupplierModel::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->word,
        'slug' => $faker->word,
        'images' => $faker->word,
        'intro' => $faker->word,
        'desc' => $faker->word,
    ];
});

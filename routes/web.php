<?php
use App\ValUrl;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    $a = new ValUrl();
//
//    echo "<pre>";
//    print_r($a->checkURRL('http://google.com'));
//    echo "</pre>";
//
//    echo "<pre>";
//    print_r($a->subString('http://google.com/a/','/xyz.html '));
//    echo "</pre>";
//
//    echo "<pre>";
//    print_r($a->protocol('http://google.com'));
//    echo "</pre>";
//
//    echo "<pre>";
//    print_r($a->port('http://google.com'));
//    echo "</pre>";
//
//    echo "<pre>";
//    print_r($a->domain('http://google.com'));
//    echo "</pre>";
////    dd($a->checkURRL('http://google.com'));
////    dd($a->protocol('http://google.com'));
////    dd($a->subString('http://google.com/a/',' /xyz.html '));
    return view('welcome');
//


});

Route::get('lang/{lang}','Admin\LangController@lang')->name('lang');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/accessors',function (){
    $user = \App\User::find(1);
    return $upper_name = $user->name;
});

Route::get('/mutators',function (){
    $user = \App\User::find(1);

    $user->password = "123456";

    return $user->password;
});

Route::prefix('admin')->group(function (){

    Route::get('/product','Admin\ProductController@index');
    Route::get('/product/create','Admin\ProductController@create');
    Route::get('/product/edit/{id}','Admin\ProductController@edit');
    Route::get('/product/delete/{id}','Admin\ProductController@delete');

    Route::post('/product','Admin\ProductController@store');
    Route::post('/product/{id}','Admin\ProductController@update');
    Route::post('/product/delete/{id}','Admin\ProductController@delete');


});



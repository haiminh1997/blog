@extends('admin.layouts.layouts');

@section("title")
    Sản phẩm
    @endsection

@section("content")
    <div class="container">
        <h2>Danh sách sản phẩm</h2>
        <p><a href="{{ secure_url('admin/product/create') }}" class="btn btn-success">Thêm sản phẩm</a></p>
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Tên</th>
                <th>Slug</th>
                <th>Hình ảnh</th>
                <th>Mô tả ngắn</th>
                <th>Mô tả</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->slug }}</td>
                    <td>{{ $product->images }}</td>
                    <td>{{ $product->intro }}</td>
                    <td>{{ $product->desc }}</td>
                    <td>
                        <a href="{{ secure_url('admin/product/delete/'.$product->id) }}" class="btn btn-danger">Xóa</a>
                        <a href="{{ secure_url('admin/product/edit'.$product->id) }}" class="btn btn-warning">Sửa</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
    </div>
    @endsection

@extends('admin.layouts.layouts);
@section("title")
    Xóa sp
    @endsection
@section("content")

    <div class="container">
        <form name="product" method="post" action="{{ secure_url("admin/product/delete/$id") }}">

            {{ csrf_field() }}

            <div class="form-group">
                <label>Tên sp: {{$product->name}}</label>
            </div>

            <button type="submit" class="btn btn-default">Xóa</button>
        </form>
    </div>
    @endsection

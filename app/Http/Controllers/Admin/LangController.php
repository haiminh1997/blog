<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LangController extends Controller
{
    private $langActive = [ // mảng các ngôn ngữ
      'vi',
      'en'
    ];
    public function lang(Request $request,$lang){
        if(in_array($lang,$this->langActive)){ // nếu trong mảng có ngôn ngữ
            $request->session()->put(['lang' => $lang]); // lưu vào session
            return redirect()->back();
        }
    }
}

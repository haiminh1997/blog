<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    //
    public $table = 'products';
}
